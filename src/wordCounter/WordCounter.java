package wordCounter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class WordCounter {
	private String message;
	private Map<String,Integer> wordCounter;
	public WordCounter(String message){
		this.message = message;
		wordCounter = new HashMap<String,Integer>();
	}
	public void count(){
		for(String s : message.split(" ")){
			if(wordCounter.containsKey(s)==true){
				wordCounter.put(s,wordCounter.get(s)+1);
			}
			else{
				wordCounter.put(s,1);
			}
		}
	}
	public int hasWord(String word){
		System.out.println(wordCounter);
		if(wordCounter.containsKey(word)==false){
			return 0;
		}
		return wordCounter.get(word);
	}
	
}

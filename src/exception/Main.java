package exception;

public class Main {
	public static void main(String[] args){
		Refrigerator refrigerator = new Refrigerator();
		try{
			refrigerator.put("pessi");
			refrigerator.put("cake");
			refrigerator.put("apple");
			refrigerator.put("water");
		}
		catch(FullException e){
			System.err.println("Error:"+e.getMessage());
		}
		System.out.println(refrigerator.toString());
	}
}

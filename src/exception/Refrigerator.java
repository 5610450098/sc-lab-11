package exception;

import java.util.ArrayList;

public class Refrigerator {
	private int size;
	private ArrayList<String> things;
	
	public Refrigerator(){
		things = new ArrayList<String>();
		size = 3;
	}
	public void put(String stuff) throws FullException {
		if(things.size()==size){
			throw new FullException("Refrigerator Full"); 
		}
		things.add(stuff);
	}
	public String takeOut(String stuff){
		if(things.contains(stuff)){
			things.remove(things.indexOf(stuff));
			return stuff;
		}
		return null;
	}
	public String toString(){
		String str = "";
		for(String s:things){
			str+=s+" ";
		}
		return str;
	}


}
